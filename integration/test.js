const models = require('../db/models');
const RPCService = require('./RPCService');

const test = () => {
  RPCService.consumeToQueue('test', async (jsonMessage, ack, queue) => {
    const { message } = jsonMessage
    ack();
    console.log(`Message received: ${message}`)
    try {
      const testObject = 'test object' // Supone que es una query a base de datos que puede fallar
      if (!testObject)
        return RPCService.replyToQueue(queue, { status: 404, details: "object not found" })
      return RPCService.replyToQueue(queue, { status: 200, details: "object found", data: testObject })
    } catch (err) {
      console.log(err)
      return res.status(500).send(err)
    }

  });
};

module.exports = test;
