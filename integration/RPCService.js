require('dotenv').config();
const amqplib = require('amqplib')
let channel;
const { v4: uuidvv4 } = require('uuid')
const RPCService = module.exports
const timeout = require('../utils/timeout')
const TIMEOUT_MS = 4000
const PREFETCH_COUNT = 200

const initialize = async (queueName, cb) => {
  try {
    const connection = await amqplib.connect(process.env.RABBITMQ);
    channel = await connection.createChannel();
    console.log(queueName, ' connection initialized')
    return cb()
  } catch (e) {
    console.log("INITIALIZE CATCH", e)
  }
}
RPCService.replyToQueue = async (queue, data) => {
  const msg = JSON.stringify(data);
  if (!channel)
    return initialize(queue.name, () => {
      channel.sendToQueue(
        queue.replyTo,
        Buffer.from(msg),
        { correlationId: queue.id }
      )
    })

  channel.sendToQueue(
    queue.replyTo,
    Buffer.from(msg),
    { correlationId: queue.id }
  )
};

RPCService.publishToQueue = async (queueName, data) => {
  const uuid = uuidvv4();
  if (!channel) {
    return new Promise(async (resolve, reject) => {
      await initialize(queueName, async () => {
        const q = await channel.assertQueue('', { exclusive: true });
        channel.prefetch(PREFETCH_COUNT);
        channel.sendToQueue(queueName, Buffer.from(JSON.stringify(data)), {
          replyTo: q.queue,
          correlationId: uuid
        });
        await Promise.all([
          await channel.consume(q.queue, async msg => {

            channel.ack(msg)
            if (msg.properties.correlationId == uuid) {
              resolve(JSON.parse(msg.content))
            }

          }, { noAck: false }),
          timeout(TIMEOUT_MS)

        ])

      })
      reject(`RabbitMQ - ${queueName} has not been initialized`)
    })
  }
  return new Promise(async (resolve, reject) => {
    const q = await channel.assertQueue('', { exclusive: true });
    channel.prefetch(PREFETCH_COUNT);

    channel.sendToQueue(queueName, Buffer.from(JSON.stringify(data)), {
      replyTo: q.queue,
      correlationId: uuid
    });
    await Promise.all([
      await channel.consume(q.queue, async msg => {
        channel.ack(msg)

        if (msg.properties.correlationId == uuid) {
          resolve(JSON.parse(msg.content))
        }

      }, { noAck: false }),
      timeout(TIMEOUT_MS)

    ])
    reject(`RabbitMQ - ${queueName} has not been initialized`)

  })

}

RPCService.consumeToQueue = async (queueName, cb) => {
  if (!channel)
    return initialize(queueName, () => {
      channel.assertQueue(queueName)
        .then(() => {
          channel.prefetch(PREFETCH_COUNT);

          channel.consume(queueName, msg => {
            const content = msg.content.toString();
            try {
              const obj = JSON.parse(content);
              const ack = () => channel.ack(msg);
              cb(obj, ack, { name: queueName, replyTo: msg.properties.replyTo, id: msg.properties.correlationId });
            } catch (err) {
              console.log(err);
              console.log(content);
              channel.ack(msg);
            };
          });
        });
    });

  channel.consume(queueName, msg => {
    const content = msg.content.toString();
    try {
      const obj = JSON.parse(content);

      const ack = () => channel.ack(msg);
      cb(obj, ack, { name: queueName, replyTo: msg.properties.replyTo, id: msg.properties.correlationId });
    } catch (err) {
      console.log(err);
      console.log(content);
      channel.ack(msg);
    };
  })
}

process.on('exit', (code) => {
  channel.close();
  console.log(`Closing rabbitmq channel`);
});
