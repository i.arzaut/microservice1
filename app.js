require('dotenv').config();
const cors = require('cors');
const express = require('express');
const app = express();
const admin = require('firebase-admin/app');
const corsOptions = require('./config/cors');
const customer = require('./route/customer');
const { checkJwt } = require('./middlewares/jwt');
const testQueue = require('./integration/test')
testQueue()
admin.initializeApp({
  credential: admin.cert('./simple-comercio-firebase-admin.json'),
  databaseURL: 'https://simple-comercio.firebaseio.com',
});

app.use(cors(corsOptions));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());


app.use(checkJwt);
app.use('/api/customer', customer);

const port = process.env.APP_PORT || 4008;
app.listen(port, () => {
  console.log(`Account Service listening on port ${port}`);
});
