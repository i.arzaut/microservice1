const admin = require("firebase-admin");

const checkJwt = (req, res, next) => {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];
  if (token == null) return res.sendStatus(401);
  return admin
    .auth()
    .verifyIdToken(token)
    .then((decodedToken) => {
      if (decodedToken.firebase.sign_in_provider === 'password' && !decodedToken.email_verified)
        return res.status(401).json({ details: 'El email no ha sido verificado' });
      req.user = decodedToken;
      next();
    })
    .catch((error) => {
      console.log(error);
      return res.sendStatus(401);
    });
};

const jwtAuthz = (claims) => {
  return (req, res, next) => {
    let includeClaims = false;
    claims.forEach(claim => {
      if (req.user[claim] === true)
        return includeClaims = true;
    });
    if (includeClaims) return next();

    return res.sendStatus(403);
  }
};

module.exports = { checkJwt, jwtAuthz };