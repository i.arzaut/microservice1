require('dotenv').config();
const express = require('express');

const models = require('../db/models')
const { Customer } = require('../db/models');
const RPCService = require('../integration/RPCService')
const { jwtAuthz } = require('../middlewares/jwt');
const router = express.Router();


module.exports = router;
